﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PropagationRemasteredBeta
{
    public partial class SimulationVue : Form
    {
        Terrain t;
        Bitmap BufferImage;
        int frameCount;
        bool started = false;
        Stopwatch stopwatch;
        double totalTime;

        public Size terrainSize;
        public int infectedProportion;
        public int resistantProportion;
        public int deathRate;
        public int infectionRate;
        public int cureTime;
        public bool needToSave;
        public string savesLocation;
        public string simulationName;

        public SimulationVue(Size terrainSize, int infectedProportion, int resistantProportion, int deathRate, int infectionRate, int cureTime, bool needToSave, string savesLocation, string simulationName)
        {
            InitializeComponent();
            this.terrainSize = terrainSize;
            this.infectedProportion = infectedProportion;
            this.resistantProportion = resistantProportion;
            this.deathRate = deathRate;
            this.infectionRate = infectionRate;
            this.cureTime = cureTime;
            this.needToSave = needToSave;
            this.savesLocation = savesLocation;
            this.simulationName = simulationName;

            frameCount = 0;
            BufferImage = new Bitmap(terrainSize.Width, terrainSize.Height);
            stopwatch = new Stopwatch();
            totalTime = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnStartOneForAll.Enabled = false;
            StartStop(1);
        }

        private void StartStop(int mode)
        {
            if (started)
            {
                started = false;
                btnStartNormal.Text = "Start classic";
                btnStartOneForAll.Text = "Start one for all";
                btnStartOneForAll.Enabled = true;
                btnStartNormal.Enabled = true;
                tmrTick.Stop();
            }
            else
            {
                started = true;
                btnStartNormal.Text = "Stop";
                btnStartOneForAll.Text = "Stop";
                tmrTick.Start();
                t = new Terrain(terrainSize, infectedProportion, resistantProportion, deathRate, infectionRate, cureTime);
                frameCount = 0;

                switch (mode)
                {
                    case 1:
                        t.Generate();
                        break;
                    case 2:
                        t.GenerateOneForAll();
                        break;
                }

            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void tmrTick_Tick(object sender, EventArgs e)
        {
            tmrTick.Stop();
            //t.Refresh();
            stopwatch.Start();
            int infecteds;
            if (t.InfectedCount.Count > 0)
            {
                infecteds = t.InfectedCount[t.InfectedCount.Count() - 1];
            }
            else
            {
                infecteds = 1;
            }
            if (infecteds > 0)
            {
                t.Refresh();
            }
            else
            {
                StartStop(1);
                //to do a last display after finished
                BufferImage = t.Display();
                pbxTerrain.Image = BufferImage;
            }
            //this.Invalidate();

            BufferImage = t.Display();

            tmrTick.Start();
            if (needToSave)
            {
                Save(BufferImage, frameCount);
            }
            pbxTerrain.Image = BufferImage;
            stopwatch.Stop();
            totalTime = stopwatch.ElapsedMilliseconds;
            frameCount++;
            lblFrameCount.Text = frameCount.ToString();
            lblTotalTime.Text = totalTime.ToString();


            //refresh all the stats
            lblMemory.Text = GetMemoryUsage().ToString() + " Mb";
            lblelementsCounter.Text = t.DRAWED_ELEMENTS.ToString();
            //lblInfected.Text = t.InfectedCount[t.InfectedCount.Count() - 1].ToString();
            //lblImmunes.Text = t.ImmuneCount[t.ImmuneCount.Count() - 1].ToString();
            //lblSain.Text = t.SainCount[t.SainCount.Count() - 1].ToString();
            //lblDeaths.Text = t.DeadCount[t.DeadCount.Count() - 1].ToString();
        }
        private void Save(Bitmap imageToSave, int frameNumber)
        {
            string path = "./" + savesLocation + "/" + simulationName + "/";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //only save if the directory has been created successfully
            imageToSave.Save(path + simulationName + "_" + frameNumber + ".png", System.Drawing.Imaging.ImageFormat.Png);
        }
        /// <summary>
        /// Code found on Stack Overflow, dont quote me on it
        /// </summary>
        /// <returns></returns>
        private double GetMemoryUsage()
        {
            var memory = 0.0;
            using (Process proc = Process.GetCurrentProcess())
            {
                // The proc.PrivateMemorySize64 will returns the private memory usage in byte.
                // Would like to Convert it to Megabyte? divide it by 2^20
                memory = proc.PrivateMemorySize64 / (1024 * 1024);
            }
            return memory;
        }

        private void btnStartOneForAll_Click(object sender, EventArgs e)
        {
            btnStartNormal.Enabled = false;
            StartStop(2);
        }

        private void pbxTerrain_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SimulationVue_Load(object sender, EventArgs e)
        {
            lblSimulationName.Text = simulationName;
        }
    }
}
