﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PropagationRemasteredBeta
{
    public partial class MainVue : Form
    {
        public Size terrainSize;
        public int infectedProportion;
        public int resistantProportion;
        public int deathRate;
        public int infectionRate;
        public int cureTime;
        public bool needToSave;
        public string savesLocation;
        public string simulationName;
        public MainVue()
        {
            InitializeComponent();
        }

        private void MainVue_Load(object sender, EventArgs e)
        {

        }

        private void btnRunSimulation_Click(object sender, EventArgs e)
        {
            terrainSize = new Size(Convert.ToInt32(nupPopulationSize.Value), Convert.ToInt32(nupPopulationSize.Value));
            infectedProportion = Convert.ToInt32(nupInfecteds.Value);
            resistantProportion = Convert.ToInt32(nupImmunes.Value);
            deathRate = Convert.ToInt32(nupMortality.Value);
            infectionRate = Convert.ToInt32(nupInfectiosity.Value);
            cureTime = Convert.ToInt32(nupCureTime.Value);
            needToSave = chkbxSaveFiles.Checked;
            simulationName = tbxSimulationName.Text;
            savesLocation = tbxSaveFilePath.Text;

            SimulationVue sm = new SimulationVue(terrainSize, infectedProportion, resistantProportion, deathRate, infectionRate, cureTime, needToSave, savesLocation, simulationName);
            sm.Closed += (s, args) => this.Show();
            sm.Show();
            this.Hide();
        }
        private void nupPopulationSize_ValueChanged(object sender, EventArgs e)
        {
            decimal value = nupPopulationSize.Value;
            lblPopulationSizePrediction.Text = (value * value).ToString();
        }
        private void Refresh()
        {
            nupInfecteds.Value = infectedProportion;
            nupImmunes.Value = resistantProportion;
            nupMortality.Value = deathRate;
            nupInfectiosity.Value = infectionRate;
            nupCureTime.Value = cureTime;
        }
        private void btnRandomizeStats_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            infectedProportion = rnd.Next(0, 80);
            resistantProportion = rnd.Next(0, 80);
            deathRate = rnd.Next(0, 80);
            infectionRate = rnd.Next(0, 80);
            cureTime = rnd.Next(0, 80);

            Refresh();
        }
        private void btnOpenSimViewer_Click(object sender, EventArgs e)
        {

        }
    }
}
