﻿namespace PropagationRemasteredBeta
{
    partial class SimulationVue
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStartNormal = new System.Windows.Forms.Button();
            this.pbxTerrain = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tmrTick = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblMemory = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblelementsCounter = new System.Windows.Forms.Label();
            this.lblFrameCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTotalTime = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lblDeaths = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lblSain = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lblImmunes = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblInfected = new System.Windows.Forms.Label();
            this.btnStartOneForAll = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblSimulationName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTerrain)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStartNormal
            // 
            this.btnStartNormal.Location = new System.Drawing.Point(1091, 362);
            this.btnStartNormal.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartNormal.Name = "btnStartNormal";
            this.btnStartNormal.Size = new System.Drawing.Size(140, 63);
            this.btnStartNormal.TabIndex = 0;
            this.btnStartNormal.Text = "Start classic";
            this.btnStartNormal.UseVisualStyleBackColor = true;
            this.btnStartNormal.Click += new System.EventHandler(this.button1_Click);
            // 
            // pbxTerrain
            // 
            this.pbxTerrain.Location = new System.Drawing.Point(16, 59);
            this.pbxTerrain.Margin = new System.Windows.Forms.Padding(4);
            this.pbxTerrain.Name = "pbxTerrain";
            this.pbxTerrain.Size = new System.Drawing.Size(1067, 985);
            this.pbxTerrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxTerrain.TabIndex = 1;
            this.pbxTerrain.TabStop = false;
            this.pbxTerrain.Click += new System.EventHandler(this.pbxTerrain_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1091, 108);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Frames";
            // 
            // tmrTick
            // 
            this.tmrTick.Interval = 10;
            this.tmrTick.Tick += new System.EventHandler(this.tmrTick_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1091, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Memory Usage";
            // 
            // lblMemory
            // 
            this.lblMemory.AutoSize = true;
            this.lblMemory.Location = new System.Drawing.Point(1091, 31);
            this.lblMemory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMemory.Name = "lblMemory";
            this.lblMemory.Size = new System.Drawing.Size(35, 16);
            this.lblMemory.TabIndex = 3;
            this.lblMemory.Text = "NaN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1091, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Drawed Elemets";
            // 
            // lblelementsCounter
            // 
            this.lblelementsCounter.AutoSize = true;
            this.lblelementsCounter.Location = new System.Drawing.Point(1091, 75);
            this.lblelementsCounter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblelementsCounter.Name = "lblelementsCounter";
            this.lblelementsCounter.Size = new System.Drawing.Size(35, 16);
            this.lblelementsCounter.TabIndex = 5;
            this.lblelementsCounter.Text = "NaN";
            // 
            // lblFrameCount
            // 
            this.lblFrameCount.AutoSize = true;
            this.lblFrameCount.Location = new System.Drawing.Point(1091, 124);
            this.lblFrameCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrameCount.Name = "lblFrameCount";
            this.lblFrameCount.Size = new System.Drawing.Size(35, 16);
            this.lblFrameCount.TabIndex = 7;
            this.lblFrameCount.Text = "NaN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1091, 151);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total Time (ms) :";
            // 
            // lblTotalTime
            // 
            this.lblTotalTime.AutoSize = true;
            this.lblTotalTime.Location = new System.Drawing.Point(1091, 167);
            this.lblTotalTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalTime.Name = "lblTotalTime";
            this.lblTotalTime.Size = new System.Drawing.Size(35, 16);
            this.lblTotalTime.TabIndex = 9;
            this.lblTotalTime.Text = "NaN";
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Location = new System.Drawing.Point(1091, 326);
            this.lbl6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(54, 16);
            this.lbl6.TabIndex = 10;
            this.lbl6.Text = "Deads :";
            // 
            // lblDeaths
            // 
            this.lblDeaths.AutoSize = true;
            this.lblDeaths.Location = new System.Drawing.Point(1091, 342);
            this.lblDeaths.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeaths.Name = "lblDeaths";
            this.lblDeaths.Size = new System.Drawing.Size(35, 16);
            this.lblDeaths.TabIndex = 11;
            this.lblDeaths.Text = "NaN";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(1091, 282);
            this.lbl5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(47, 16);
            this.lbl5.TabIndex = 12;
            this.lbl5.Text = "Sains :";
            // 
            // lblSain
            // 
            this.lblSain.AutoSize = true;
            this.lblSain.Location = new System.Drawing.Point(1091, 298);
            this.lblSain.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSain.Name = "lblSain";
            this.lblSain.Size = new System.Drawing.Size(35, 16);
            this.lblSain.TabIndex = 13;
            this.lblSain.Text = "NaN";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(1091, 239);
            this.lbl4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(67, 16);
            this.lbl4.TabIndex = 14;
            this.lbl4.Text = "Immunes :";
            // 
            // lblImmunes
            // 
            this.lblImmunes.AutoSize = true;
            this.lblImmunes.Location = new System.Drawing.Point(1091, 255);
            this.lblImmunes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImmunes.Name = "lblImmunes";
            this.lblImmunes.Size = new System.Drawing.Size(35, 16);
            this.lblImmunes.TabIndex = 15;
            this.lblImmunes.Text = "NaN";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1091, 196);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 16);
            this.label12.TabIndex = 16;
            this.label12.Text = "Infecteds :";
            // 
            // lblInfected
            // 
            this.lblInfected.AutoSize = true;
            this.lblInfected.Location = new System.Drawing.Point(1091, 212);
            this.lblInfected.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInfected.Name = "lblInfected";
            this.lblInfected.Size = new System.Drawing.Size(35, 16);
            this.lblInfected.TabIndex = 17;
            this.lblInfected.Text = "NaN";
            // 
            // btnStartOneForAll
            // 
            this.btnStartOneForAll.Location = new System.Drawing.Point(1091, 432);
            this.btnStartOneForAll.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartOneForAll.Name = "btnStartOneForAll";
            this.btnStartOneForAll.Size = new System.Drawing.Size(140, 63);
            this.btnStartOneForAll.TabIndex = 18;
            this.btnStartOneForAll.Text = "Start oneForAll";
            this.btnStartOneForAll.UseVisualStyleBackColor = true;
            this.btnStartOneForAll.Click += new System.EventHandler(this.btnStartOneForAll_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1091, 503);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 63);
            this.button1.TabIndex = 19;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lblSimulationName
            // 
            this.lblSimulationName.AutoSize = true;
            this.lblSimulationName.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSimulationName.Location = new System.Drawing.Point(12, 13);
            this.lblSimulationName.Name = "lblSimulationName";
            this.lblSimulationName.Size = new System.Drawing.Size(261, 34);
            this.lblSimulationName.TabIndex = 20;
            this.lblSimulationName.Text = "Simulation Name";
            // 
            // SimulationVue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 1052);
            this.Controls.Add(this.lblSimulationName);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnStartOneForAll);
            this.Controls.Add(this.lblInfected);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblImmunes);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lblSain);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lblDeaths);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lblTotalTime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblFrameCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblelementsCounter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblMemory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbxTerrain);
            this.Controls.Add(this.btnStartNormal);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SimulationVue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulation";
            this.Load += new System.EventHandler(this.SimulationVue_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pbxTerrain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartNormal;
        private System.Windows.Forms.PictureBox pbxTerrain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrTick;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMemory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblelementsCounter;
        private System.Windows.Forms.Label lblFrameCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTotalTime;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lblDeaths;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lblSain;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lblImmunes;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblInfected;
        private System.Windows.Forms.Button btnStartOneForAll;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblSimulationName;
    }
}

