﻿
namespace PropagationRemasteredBeta
{
    partial class MainVue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnRandomName = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxSimulationName = new System.Windows.Forms.TextBox();
            this.chkbxSaveFiles = new System.Windows.Forms.CheckBox();
            this.tbxSaveFilePath = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnRandomizeStats = new System.Windows.Forms.Button();
            this.nupCureTime = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboModes = new System.Windows.Forms.ComboBox();
            this.nupImmunes = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nupInfecteds = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nupMortality = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nupInfectiosity = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPopulationSizePrediction = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nupPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRunSimulation = new System.Windows.Forms.Button();
            this.btnOpenSimViewer = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCureTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupImmunes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupInfecteds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMortality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupInfectiosity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupPopulationSize)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Verdana", 20F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(537, 41);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Propagation Remastered 2022";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnRandomName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbxSimulationName);
            this.groupBox1.Controls.Add(this.chkbxSaveFiles);
            this.groupBox1.Controls.Add(this.tbxSaveFilePath);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12F);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(534, 196);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Settings";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 10F);
            this.label11.Location = new System.Drawing.Point(381, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 20);
            this.label11.TabIndex = 19;
            this.label11.Text = "Randomize";
            // 
            // btnRandomName
            // 
            this.btnRandomName.Enabled = false;
            this.btnRandomName.Location = new System.Drawing.Point(491, 16);
            this.btnRandomName.Name = "btnRandomName";
            this.btnRandomName.Size = new System.Drawing.Size(34, 34);
            this.btnRandomName.TabIndex = 18;
            this.btnRandomName.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(179, 25);
            this.label10.TabIndex = 17;
            this.label10.Text = "Saves Location :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 25);
            this.label9.TabIndex = 16;
            this.label9.Text = "Name :";
            // 
            // tbxSimulationName
            // 
            this.tbxSimulationName.Font = new System.Drawing.Font("Verdana", 12F);
            this.tbxSimulationName.Location = new System.Drawing.Point(11, 56);
            this.tbxSimulationName.Name = "tbxSimulationName";
            this.tbxSimulationName.Size = new System.Drawing.Size(514, 32);
            this.tbxSimulationName.TabIndex = 7;
            this.tbxSimulationName.Text = "Covid34";
            // 
            // chkbxSaveFiles
            // 
            this.chkbxSaveFiles.AutoSize = true;
            this.chkbxSaveFiles.Checked = true;
            this.chkbxSaveFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxSaveFiles.Location = new System.Drawing.Point(11, 93);
            this.chkbxSaveFiles.Name = "chkbxSaveFiles";
            this.chkbxSaveFiles.Size = new System.Drawing.Size(236, 29);
            this.chkbxSaveFiles.TabIndex = 6;
            this.chkbxSaveFiles.Text = "Save the simulation";
            this.chkbxSaveFiles.UseVisualStyleBackColor = true;
            // 
            // tbxSaveFilePath
            // 
            this.tbxSaveFilePath.Font = new System.Drawing.Font("Verdana", 12F);
            this.tbxSaveFilePath.Location = new System.Drawing.Point(11, 153);
            this.tbxSaveFilePath.Name = "tbxSaveFilePath";
            this.tbxSaveFilePath.Size = new System.Drawing.Size(514, 32);
            this.tbxSaveFilePath.TabIndex = 5;
            this.tbxSaveFilePath.Text = "Saves";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.btnRandomizeStats);
            this.groupBox2.Controls.Add(this.nupCureTime);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.comboModes);
            this.groupBox2.Controls.Add(this.nupImmunes);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.nupInfecteds);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.nupMortality);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.nupInfectiosity);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblPopulationSizePrediction);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.nupPopulationSize);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 12F);
            this.groupBox2.Location = new System.Drawing.Point(12, 268);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(534, 342);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Simulation Settings";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 10F);
            this.label12.Location = new System.Drawing.Point(381, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 20);
            this.label12.TabIndex = 21;
            this.label12.Text = "Randomize";
            // 
            // btnRandomizeStats
            // 
            this.btnRandomizeStats.Location = new System.Drawing.Point(491, 19);
            this.btnRandomizeStats.Name = "btnRandomizeStats";
            this.btnRandomizeStats.Size = new System.Drawing.Size(34, 34);
            this.btnRandomizeStats.TabIndex = 20;
            this.btnRandomizeStats.UseVisualStyleBackColor = true;
            this.btnRandomizeStats.Click += new System.EventHandler(this.btnRandomizeStats_Click);
            // 
            // nupCureTime
            // 
            this.nupCureTime.Location = new System.Drawing.Point(14, 297);
            this.nupCureTime.Name = "nupCureTime";
            this.nupCureTime.Size = new System.Drawing.Size(236, 32);
            this.nupCureTime.TabIndex = 15;
            this.nupCureTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 25);
            this.label8.TabIndex = 14;
            this.label8.Text = "Cure time :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(287, 269);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 25);
            this.label7.TabIndex = 13;
            this.label7.Text = "Mode :";
            // 
            // comboModes
            // 
            this.comboModes.FormattingEnabled = true;
            this.comboModes.Items.AddRange(new object[] {
            "Classic",
            "One for all"});
            this.comboModes.Location = new System.Drawing.Point(292, 297);
            this.comboModes.Name = "comboModes";
            this.comboModes.Size = new System.Drawing.Size(236, 33);
            this.comboModes.TabIndex = 12;
            // 
            // nupImmunes
            // 
            this.nupImmunes.Location = new System.Drawing.Point(292, 234);
            this.nupImmunes.Name = "nupImmunes";
            this.nupImmunes.Size = new System.Drawing.Size(236, 32);
            this.nupImmunes.TabIndex = 11;
            this.nupImmunes.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(287, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Immunes (%) :";
            // 
            // nupInfecteds
            // 
            this.nupInfecteds.Location = new System.Drawing.Point(14, 234);
            this.nupInfecteds.Name = "nupInfecteds";
            this.nupInfecteds.Size = new System.Drawing.Size(236, 32);
            this.nupInfecteds.TabIndex = 9;
            this.nupInfecteds.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(167, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Infecteds (%) :";
            // 
            // nupMortality
            // 
            this.nupMortality.Location = new System.Drawing.Point(292, 171);
            this.nupMortality.Name = "nupMortality";
            this.nupMortality.Size = new System.Drawing.Size(236, 32);
            this.nupMortality.TabIndex = 7;
            this.nupMortality.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(287, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mortality (%):";
            // 
            // nupInfectiosity
            // 
            this.nupInfectiosity.Location = new System.Drawing.Point(14, 171);
            this.nupInfectiosity.Name = "nupInfectiosity";
            this.nupInfectiosity.Size = new System.Drawing.Size(236, 32);
            this.nupInfectiosity.TabIndex = 5;
            this.nupInfectiosity.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(187, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Infectiosity (%) :";
            // 
            // lblPopulationSizePrediction
            // 
            this.lblPopulationSizePrediction.AutoSize = true;
            this.lblPopulationSizePrediction.Location = new System.Drawing.Point(260, 104);
            this.lblPopulationSizePrediction.Name = "lblPopulationSizePrediction";
            this.lblPopulationSizePrediction.Size = new System.Drawing.Size(95, 25);
            this.lblPopulationSizePrediction.TabIndex = 3;
            this.lblPopulationSizePrediction.Text = "160\'000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Simulated individuals :";
            // 
            // nupPopulationSize
            // 
            this.nupPopulationSize.Location = new System.Drawing.Point(14, 69);
            this.nupPopulationSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nupPopulationSize.Name = "nupPopulationSize";
            this.nupPopulationSize.Size = new System.Drawing.Size(514, 32);
            this.nupPopulationSize.TabIndex = 1;
            this.nupPopulationSize.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nupPopulationSize.ValueChanged += new System.EventHandler(this.nupPopulationSize_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Size :";
            // 
            // btnRunSimulation
            // 
            this.btnRunSimulation.Font = new System.Drawing.Font("Verdana", 12F);
            this.btnRunSimulation.Location = new System.Drawing.Point(12, 616);
            this.btnRunSimulation.Name = "btnRunSimulation";
            this.btnRunSimulation.Size = new System.Drawing.Size(534, 83);
            this.btnRunSimulation.TabIndex = 8;
            this.btnRunSimulation.Text = "Run the simulation";
            this.btnRunSimulation.UseVisualStyleBackColor = true;
            this.btnRunSimulation.Click += new System.EventHandler(this.btnRunSimulation_Click);
            // 
            // btnOpenSimViewer
            // 
            this.btnOpenSimViewer.Font = new System.Drawing.Font("Verdana", 12F);
            this.btnOpenSimViewer.Location = new System.Drawing.Point(12, 705);
            this.btnOpenSimViewer.Name = "btnOpenSimViewer";
            this.btnOpenSimViewer.Size = new System.Drawing.Size(534, 83);
            this.btnOpenSimViewer.TabIndex = 9;
            this.btnOpenSimViewer.Text = "Open SimViewer";
            this.btnOpenSimViewer.UseVisualStyleBackColor = true;
            this.btnOpenSimViewer.Click += new System.EventHandler(this.btnOpenSimViewer_Click);
            // 
            // MainVue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 798);
            this.Controls.Add(this.btnOpenSimViewer);
            this.Controls.Add(this.btnRunSimulation);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTitle);
            this.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainVue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainVue";
            this.Load += new System.EventHandler(this.MainVue_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCureTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupImmunes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupInfecteds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMortality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupInfectiosity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupPopulationSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkbxSaveFiles;
        private System.Windows.Forms.TextBox tbxSaveFilePath;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPopulationSizePrediction;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nupPopulationSize;
        private System.Windows.Forms.NumericUpDown nupImmunes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nupInfecteds;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nupMortality;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nupInfectiosity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboModes;
        private System.Windows.Forms.NumericUpDown nupCureTime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnRunSimulation;
        private System.Windows.Forms.Button btnOpenSimViewer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxSimulationName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnRandomName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnRandomizeStats;
    }
}